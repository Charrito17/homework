package javabasics;

import java.util.Arrays;
import java.util.Scanner;

public class GradeStatistics {
	//
	public static int[] grades;

	public static void main(String[] args) {
		Grades();
		printArray(grades);
		System.out.println("Average: \t" + average(grades));
		System.out.println("Median: \t" + median(grades));
		System.out.println("Max: \t" + max(grades));
		System.out.println("Min: \t" + min(grades));
		System.out.println("Standard Deviation: \t" + standardDev(grades));
	}

	// Method
	public static void Grades() {
		Scanner Scan = new Scanner(System.in);
		System.out.println("Enter Number of Student:");

		int numofStudents = Scan.nextInt();
		grades = new int[numofStudents];

		for (int i = 0; i < numofStudents; i++) {
			System.out.println("Enter Student " + (i + 1) + " " + "Grade");
			grades[i] = Scan.nextInt();
			System.out.println(grades[i]);
		}

	}

	// Method
	public static void printArray(int[] array) {

	}

	// Method
	public static double average(int[] array) {
		double sum = 0;
		for (int i = 0; i < grades.length; i++) {
			sum += grades[i];
		}
		return (sum / grades.length);
	}

	// Method
	public static double median(int[] array) {
		int mid = grades.length / 2;
		if (grades.length % 2 == 1) {
			return grades[mid];
		}
		return (grades[mid - 1] + grades[mid]) / 2;
	}

	// Method
	public static int max(int[] array) {
		int max = grades[0];
		for (int i = 0; i < grades.length; i++) {
			if (max < grades[i]) {
				max = grades[i];
			}
		}
		return max;
	}

	// Method
	public static int min(int[] array) {
		int min = grades[0];
		for (int i = 0; i < grades.length; i++) {
			if (min > grades[i]) {
				grades[i] = min;
			}
		}
		return min;
	}

	// Method
	public static double standardDev(int[] array) {
		double average = average(array);
		double total = 0;
		for (int i = 0; i < grades.length; i++) {
			total += grades[i] * grades[i] - average * average;
		}
		return Math.sqrt(total / grades.length);
	}

}
