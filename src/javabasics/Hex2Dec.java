package javabasics;

import java.util.Scanner;

public class Hex2Dec {
	public static void main(String[] args) {
		Scanner Scan = new Scanner(System.in);
// Fields
		String hexDecString;
		int decimal;

		System.out.print("Enter a hex number:\t");
		hexDecString = Scan.nextLine();

		decimal = hex2Dec(hexDecString);
		System.out.print("The Decimal number is:\t" + decimal);

	}

// Method
	public static int hex2Dec(String character) {
		String varaiables = "0123456789ABCDEF";

		character = character.toUpperCase();
		int total = 0;
		
		int x = 0;
		while (x < character.length()) {
			char characters = character.charAt(x);
			int digits = varaiables.indexOf(characters);
			total = ((16 * total) + digits);
			x++;
		}

		return total;
	}

}
