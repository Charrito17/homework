package javabasics;

public class PrintPattern {
	public static void main(String[] args) {
		for (int col = 0; col <= 8; col++) { // A
			System.out.println();
			for (int row = 0; row <= col; row++) {
				System.out.print("#");
			}
		}
		System.out.print("\n");

		System.out.println("This is B");

		for (int rows = 0; rows <= 8; rows++) { // B
			System.out.println();
			for (int colo = 8; colo >= rows; colo--) {
				System.out.print("#");
			}
		}
		System.out.print("\n");
		System.out.println("This is C");

		for (int rows = 0; rows < 8; rows++) { // C
			for (int colo = 0; colo < rows; colo++) {
				System.out.print("  ");
			}
			for (int colo = rows; colo < 8; colo++) {
				System.out.print("# ");
			}
			System.out.println();
		}

		System.out.println("This is D");

		for (int row = 8; row > 0; row--) { // D
			for (int colo = 0; colo < row - 1; colo++) {
				System.out.print("  ");
			}
			for (int colo = row; colo < 8; colo++) {
				System.out.print(" #");
			}
			System.out.println();
		}
		System.out.println("This is E");
		for (int row = 0; row <= 4; row++) {
			for (int upperLine = 0; upperLine == row; upperLine++) {
				System.out.println("# # # # # # #");
			}

			System.out.println("#           #");

			for (int bottomLine = 4; bottomLine == row; bottomLine++) {
				System.out.print("# # # # # # #");
			}
		}
		System.out.println();
		System.out.println("This is F");
		for (int row = 0; row <= 4; row++) {
			for (int top = 0; top == row; top++) {
				System.out.println("# # # # # # #");
			}

			for (int space = 0; space < row; space++) {
				System.out.print("  ");
			}

			System.out.println("  #");

			for (int bottom = 4; bottom == row; bottom++) {
				System.out.print("# # # # # # #");

			}
		}
		System.out.println();
		System.out.println("This is G");
		for (int row = 0; row <= 4; row++) {
			for (int top = 0; top == row; top++) {
				System.out.println("# # # # # # #");
			}

			for (int space = 4; space > row; space--) {
				System.out.print("  ");
			}

			System.out.println("  #");

			for (int bottom = 4; bottom == row; bottom++) {
				System.out.print("# # # # # # #");

			}
		}
		System.out.println();
		System.out.println("This is H");
		for (int row = 1; row <= 7; row++) {
			for (int colo = 1; colo <= 7; colo++) {
				if ((row % 7 <= 1) || (row == colo) || (row + colo == 7 + 1)) {
					System.out.print("# ");
					continue;
				}
				System.out.print("  ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("This is I");
		for (int row = 1; row <= 7; row++) {
			for (int colo = 1; colo <= 7; colo++) {
				if ((row % 7 <= 1) || (colo % 7 <= 1) || (row == colo) || (row + colo == 7 + 1)) {
					System.out.print("# ");
					continue;
				}
				System.out.print("  ");
			}
			System.out.println();
		}
	}
}
