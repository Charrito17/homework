package javabasics;

public class ComputePi {
	public static void main(String[] args) {

		int maxTerm = 100000000; // number of terms used in computation
		double sum = 0.0;
		for (int term = 1; term <= maxTerm; term++) { // term = 1,2,3,... ,maxTerm
			if (term % 2 == 1) { // odd term number: add
				sum += 1.0 / (term * 2 - 1);
			} else { // even term number: subtract
				sum -= 1.0 / (term * 2 - 1);
			}
		}
		System.out.println(sum*4);
	}
}
