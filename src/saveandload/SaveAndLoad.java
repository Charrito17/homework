package saveandload;

import java.awt.BorderLayout;

import java.awt.Color;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import java.io.FileReader;

import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.io.*;

public class SaveAndLoad extends JFrame {
	// creating buttons
	JButton save = new JButton(" Save ");
	JButton load = new JButton(" Load ");

	// file chooser
	JFileChooser fileChosen = new JFileChooser();

	// fields
	PrintWriter out = null;

	// this is the text space
	JTextArea txt_Area1 = new JTextArea(3, 5);
	JTextArea txt_Area2 = new JTextArea(3, 5);

	public SaveAndLoad() {
		this.createGUI();
		this.showFrame();
	}

	private void createGUI() {
		// System.out.println("hello world");
		add(txt_Area1, BorderLayout.NORTH);
		txt_Area1.setBackground(Color.YELLOW);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());

		buttonPanel.add(save);
		buttonPanel.add(load);

		add(buttonPanel, BorderLayout.CENTER);

		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent saved) {
				// PrintWriter out = null;

				try {
					out = new PrintWriter("Homework#6.txt");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// System.out.println(txt_Area1.getText());
				out.println(txt_Area1.getText() + "\n");
				out.close();

			}

		});

		load.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent loading) {
				File file = new File("/Users/charrito/Desktop/Computer Science/Homework#6/Homework#6.txt");
				try {
					BufferedReader br = new BufferedReader(new FileReader(file));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(file.getName());

			}

		});

	}

	private void showFrame() {
		this.setSize(300, 400);
		this.setTitle("Homework #6");
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
}