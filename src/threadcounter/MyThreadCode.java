package threadcounter;

public class MyThreadCode {

	public static void main(String args[]) {
		makeThreads();
}
	public static void makeThreads() {
		// Fields

		Thread Dawit = new Thread(new ThreadClass());

		Thread Tony = new Thread(new ThreadClass());

		Thread Zeak = new Thread(new ThreadClass());

		Thread Yassin = new Thread(new ThreadClass());

		Thread Jason = new Thread(new ThreadClass());

		Dawit.start();
		Tony.start();
		Zeak.start();
		Yassin.start();
		Jason.start();

	
		
		try {

			Dawit.join();

			Tony.join();

			Zeak.join();

			Yassin.join();

			Jason.join();

		}
		catch (InterruptedException error) {

			error.printStackTrace();

		}
	}
}
