package threadcounter;

class IntCounter {

	private int number;

	public synchronized int getNumber() {

		return number;

	}

	public synchronized void setNumber(final int value) {

		this.number = value;

	}

}