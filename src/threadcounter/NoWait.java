package threadcounter;

public class NoWait {
	public static void main(String args[]) {
		noHinder();

	}

	public static void noHinder() {
		NoWait hinder = new NoWait();
		// Runnable is library in Java
		Thread X1 = new Thread(new Runnable() {

			@Override

			public void run() {

				hinder.incrementNumber();

			}

		});

		X1.start();

		Thread t2 = new Thread(new Runnable() {

			@Override

			public void run() {

				hinder.printNumber();

			}

		});

		t2.start();
	}

	int number = 0;

	// Method
	public void incrementNumber() {

		System.out.println("Counting Begins");

		for (int i = 0; i < 1000000; i++) {

			number += i;

		}

		System.out.println("Finished");

	}

	// Method
	public void printNumber() {

		System.out.println("Print I: \t" + number);

	}

}
