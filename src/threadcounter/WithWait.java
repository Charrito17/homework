package threadcounter;

public class WithWait {
	public static void main(String args[]) {
		stopThread();

	}

	static boolean stop = false;

	public static void stopThread() {
		WithWait hinder = new WithWait();

		Thread Th = new Thread(new Runnable() {

			@Override

			public void run() {

				synchronized (hinder) {

					hinder.incrementNumber();

					hinder.notify();

				}

			}

		});

		Th.start();

		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {

				synchronized (hinder) {

					while (!stop) {

						try {
							hinder.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}

				hinder.printInfo();

			}

		});

		t2.start();
	}

	int number = 0;

	public void incrementNumber() {

		System.out.println("Counting Begins");

		while (number < 2000000) {

			number++;

		}

		stop = true;

		System.out.println("Finished");

	}

	public void printInfo() {

		System.out.println("I Value" + number);

	}

}
