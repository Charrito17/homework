package bomb;

import java.util.Random;
import java.util.Scanner;

public class Bomb1 {
	// fields
	public static double getPolarDistance(PolarCord point1, PolarCord point2) {
		double x1 = point1.getR() * Math.cos(point1.getZ());
		double x2 = point2.getR() * Math.cos(point2.getZ());
		double y1 = point1.getR() * Math.sin(point1.getZ());
		double y2 = point2.getR() * Math.sin(point2.getZ());

		double distance = Math.sqrt((x2 - x1) + (y2 - y1));
		return distance;
	}

	public static void main(String[] args) {
		Random FLC = new Random();
		Scanner Jp = new Scanner(System.in);

		PolarCord[] cuidad = new PolarCord[7];

		for (int k = 0; k < cuidad.length; k++) {
			cuidad[k] = new PolarCord((FLC.nextDouble() * (100)), (FLC.nextDouble() * (100)));
			cuidad[k].display();
		}

		System.out.println("Drop your bomb? Enter x then y");
		PolarCord dropCite = new PolarCord(Double.parseDouble(Jp.nextLine()), Double.parseDouble(Jp.nextLine()));
		dropCite.display();

		System.out.println("radius of the blast?");
		double radius = Integer.parseInt(Jp.nextLine());

		for (int k = 0; k < cuidad.length; k++) {
			double distance = getPolarDistance(cuidad[k], dropCite);
			if (distance < radius) {
				System.out.println("City " + (k + 1) +" "+ "straight gone");
			} else {
				System.out.println("City " + (k + 1) +" "+ "lived another day");
			}
		}

	}

}
