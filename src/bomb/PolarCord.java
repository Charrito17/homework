package bomb;

public class PolarCord {
	private double x;
	private double y;
	private double r;
	private double z;

	// constructor
	public PolarCord(double xcord, double ycord) {
		x = Math.round(xcord * 100.0) / 100.0;
		y = Math.round(ycord * 100.0) / 100.0;
		r = Math.round((Math.sqrt((xcord * xcord) + (ycord + ycord))) * 100.0) / 100.0;
		z = Math.round((Math.atan(ycord / xcord)) * 100.0) / 100.0;
	}

	// constructor
	public PolarCord(int xcord, int ycord) {
		x = Math.round(xcord * 100.0) / 100.0;
		y = Math.round(ycord * 100.0) / 100.0;
		r = Math.round((Math.sqrt((xcord * xcord) + (ycord + ycord))) * 100.0) / 100.0;
		z = Math.round((Math.atan(ycord / xcord)) * 100.0) / 100.0;
	}

	// constructor
	public PolarCord() {

	}

	// method
	public void display() {
		System.out.println("Polar: (" + r + ", " + z + ")");
		System.out.println("Cartesian: (" + x + ", " + y + ")");
	}

	// method
	public double getR() {
		return r;
	}

	// method
	public double getZ() {
		return z;
	}

	// method
	public double getX() {
		return x;
	}

	// method
	public double getY() {
		return y;
	}

	// method
	public void setPolar(double setr, double settheta) {
		r = setr;
		z = settheta;
	}

	// method
	public double getPolarDistance(PolarCord point1, PolarCord point2) {
		double x1 = point1.getR() * Math.cos(point1.getZ());
		double x2 = point2.getR() * Math.cos(point2.getZ());
		double y1 = point1.getR() * Math.sin(point1.getZ());
		double y2 = point2.getR() * Math.sin(point2.getZ());

		double distance = Math.sqrt((x2 - x1) + (y2 - y1));
		return distance;
	}

}
