package tictactoe;

public class MainGame {
	// fields
	private char[][] board;
	private char currentPlayer;

	// Constructor
	public MainGame() {
		board = new char[3][3];
		currentPlayer = 'x';
		initializeBoard();

	}

	// Method
	public void initializeBoard() {
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				board[row][col] = '-';
			}
		}
	}

	// Getter
	public char getCurrentPlayer() {
		return currentPlayer;
	}

	// HardCode printing the outline
	public void printBoard() {
		System.out.println("-------------");

		for (int rows = 0; rows < 3; rows++) {
			System.out.print("| ");
			for (int colo = 0; colo < 3; colo++) {
				System.out.print(board[rows][colo] + " | ");
			}
			System.out.println();
			System.out.println("-------------");

		}
	}

	// check right here if nobody has one and all the space is filled up
	public boolean isBoardFull() {
		boolean isFull = true;

		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (board[row][col] == '-') {
					isFull = false;
				}
			}
		}

		return isFull;
	}

	// Check the diagonals to determine the winner
	private boolean checkDiagonals() {
		return ((checkRowCol(board[0][0], board[1][1], board[2][2]) == true)
				|| (checkRowCol(board[0][2], board[1][1], board[2][0]) == true));
	}

	//
	private boolean checkRowCol(char c1, char c2, char c3) {
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}

	// Checking the column to determine if there is a winner
	private boolean checkColumns() {
		for (int col = 0; col < 3; col++) {
			if (checkRowCol(board[0][col], board[1][col], board[2][col]) == true) {
				return true;
			}
		}
		return false;
	}

	// Checking the row to determine if there is a winner
	private boolean checkRows() {
		for (int row = 0; row < 3; row++) {
			if (checkRowCol(board[row][0], board[row][1], board[row][2]) == true) {
				return true;
			}
		}
		return false;
	}

	// This method checks the outcome to the other 3 in order to determine the
	// winner
	public boolean isWinner() {
		return (checkRows() || checkColumns() || checkDiagonals());
	}

	public void changePlayer() {
		if (currentPlayer == 'x') {
			currentPlayer = 'o';
		} else {
			currentPlayer = 'x';
		}
	}

	public boolean placeMark(int row, int col) {

		// Make sure that row and column are in bounds of the board.
		if ((row >= 0) && (row < 3)) {
			if ((col >= 0) && (col < 3)) {
				if (board[row][col] == '-') {
					board[row][col] = currentPlayer;
					return true;
				}
			}
		}

		return false;
	}

}
