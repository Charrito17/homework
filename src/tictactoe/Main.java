package tictactoe;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		MainGame game = new MainGame();

		game.printBoard();

		Scanner Scanner = new Scanner(System.in);

		do {
			System.out.println("Current board layout:");
			game.printBoard();
			int row;
			int col;
			do {
				System.out.println(
						"Player " + game.getCurrentPlayer() + ", enter an empty row and column to place your mark!");
				row = Scanner.nextInt() - 1;
				col = Scanner.nextInt() - 1;
			} while (!game.placeMark(row, col));
			game.changePlayer();
		} while (!game.isWinner() && !game.isBoardFull());

		if (game.isBoardFull() && !game.isWinner()) {
			System.out.println("Tie");
		} else {
			System.out.println("Current layout:");
			game.printBoard();
			game.changePlayer();
			System.out.println(game.getCurrentPlayer() + " Wins");
		}

	}
}