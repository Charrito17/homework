package lcm;

import java.util.Scanner;

public class LCM {
	public static void main(String[] args) {
		Scanner trevizo = new Scanner(System.in);

		System.out.println("Enter two numbers:");

		int number1 = trevizo.nextInt();

		int number2 = trevizo.nextInt();

		int answer = lcm(number1, number2);

		System.out.println("The LCM is:\t" + answer);

		int answer2 = lcd(number1, number2);

		System.out.println("The LCD is:\t" + answer2);
	}

	public static int lcm(int X, int Y) {
		// fields
		int x, max = 0, min = 0, lcm = 0;

		if (X > Y) {
			max = X;
			min = Y;
		} else {
			max = Y;
			min = X;
		}
		for (int i = 1; i < min; i++) {
			x = max * i;
			if (x % min == 0) {
				lcm = x;
				break;
			}
		}
		return lcm;
	}

	public static int lcd(int A, int B) {
		int lowest = Math.min(A, B);
		int highest = Math.max(A, B);

		int changer = lowest;

		while ((highest % changer != 0) | (lowest % changer != 0)) {
			changer--;

		}

		return changer;
	}

}
